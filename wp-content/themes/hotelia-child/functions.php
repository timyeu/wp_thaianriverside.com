<?php 
	add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
	 
	function woo_custom_cart_button_text() {
	 
	        return __( 'Thêm vào giỏ', 'woocommerce' );
	 
	}

	add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
	function custom_woocommerce_product_add_to_cart_text() {
	    global $product;    
	    $product_type = $product->product_type;  
	    switch ( $product_type ) {
	case 'variable':
	            return __( 'Lựa chọn', 'woocommerce' );
	        break;
	}
	}


	add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
	function custom_override_checkout_fields( $fields ) {
	     unset($fields['billing']['billing_company']);
	     unset($fields['billing']['billing_city']);
	     unset($fields['billing']['billing_postcode']);
	     unset($fields['billing']['billing_country']);
	     unset($fields['billing']['billing_address_2']);
	     return $fields;
	}




	add_filter( 'woocommerce_checkout_fields', 'webendev_woocommerce_checkout_fields' );
	/**
	 * Change Order Notes Placeholder Text - WooCommerce
	 * 
	 */
	function webendev_woocommerce_checkout_fields( $fields ) {

		$fields['order']['order_comments']['placeholder'] = 'Ghi chú về đơn hàng, ví dụ: lưu ý khi giao hàng.';
		return $fields;
	}


 ?>