=== THEME ===

Theme Name: Hotelia
Theme URI: http://teslathemes.com/demo/wp/hotelia/
Description: Awesome theme for a Wordpress tourist agency website
Version: 1.0
Author: TeslaThemes
Author URI: http://www.teslathemes.com/
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tags: light, theme-options,creative,responsive,unique,filtered portfolio
Requires at least: 3.6
Tested up to: 4.0
Stable tag: 1.0
License: GNU General Public License version 3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

== Description ==

Thank you for your interest shown to TeslaThemes and your purchase of Hotelia Theme. We highly appreciate this.
Hotelia is a clean, minimalist and powerful Travel WordPress Theme. It is a strong platform for a website.

FEATURES
- social media interaction
- contact form
- Google Map
- HTML 5 code
- CSS3 effects
- WordPress 4.0

DOCUMENTATION
The theme comes with an extensive documentation with snapshots to help you get started immediately.

CUSTOMIZATION
We are happy to provide you any customization you need for our themes. Don't hesitate a second to hire us for your custom project. Submit inquiries to lab@teslathemes.com.


== Installation ==

This section describes how to install the tempalte and get it working.

e.g.

1. Upload `hotelia.installation.zip` to your wordpress website through the admin panel.
2. Activate the theme after installation.
3. See the documentation file for further assistance.